#!/bin/python

##############
## Purpose of this file
#
#  This script takes the original lumber images and splits them into smaller images
#  based on the pixel dimensions and classifications in manlabel.csv.  These images
#  are then put into the folders 'sound' or 'not_sound'
#
#  FYI - This script relies on the ImageMagick CLI e.g. the 'convert' program.
#  If it's not installed on your machine, it will not return
###############

import csv
import subprocess
import os

with open('manlabel.csv', 'rb') as csvfile:
    file_reader = csv.reader(csvfile)

    heights = []
    widths = []
    for file_start, min_y, min_x, max_y, max_x, status in file_reader:

        file_name = file_start + '.png'
        width = int(max_x) - int(min_x)
        height = int(max_y) - int(min_y)
        to_crop = str(height) + 'x' + str(width) + '+' + min_x + '+' + min_y
        if status != 'sound':
            status = 'not_sound'
        file_to_save = 'data-scrubbed/train/' + status + '/' + file_start + '-' + min_y + '-' + min_x + '.png'
        command = ['convert','data/data/' + file_name, '-crop',  to_crop, file_to_save]
        subprocess.call(command)
        print command

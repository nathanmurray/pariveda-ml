from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.preprocessing.image import ImageDataGenerator
from optparse import OptionParser
from PIL import Image
import numpy as np
import os
from jinja2 import Template

IMAGE_SIZE=(50,50)
BATCH_SIZE=16

def generate_model():

    model = Sequential()
    model.add(Conv2D(16, (3, 3), input_shape=(50, 50, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(16, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
    model.add(Dense(64))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(2))
    model.add(Activation('softmax'))

    model.compile(loss='categorical_crossentropy',
                  optimizer='rmsprop',
                  metrics=['accuracy'])

    return model

def fit():
    model = generate_model()



    # this is the augmentation configuration we will use for training
    train_datagen = ImageDataGenerator(rescale=1./255)

    # this is the augmentation configuration we will use for testing:
    # only rescaling
    test_datagen = ImageDataGenerator(rescale=1./255)

    # this is a generator that will read pictures found in
    # subfolers of 'data/train', and indefinitely generate
    # batches of augmented image data
    print('generating training data')
    train_generator = train_datagen.flow_from_directory(
            'data-scrubbed/train',  # this is the target directory
            target_size=IMAGE_SIZE,  # all images will be resized to 150x150
            batch_size=BATCH_SIZE,
            class_mode='categorical')  # since we use binary_crossentropy loss, we need binary labels

    # this is a similar generator, for validation data

    print('generating validation data')
    validation_generator = test_datagen.flow_from_directory(
            'data-scrubbed/validation',
            target_size=IMAGE_SIZE,
            batch_size=BATCH_SIZE,
            class_mode='categorical')

    model.fit_generator(
            train_generator,
            steps_per_epoch=30000/BATCH_SIZE,
            epochs=50,
            validation_data=validation_generator,
            validation_steps=1000)
    model.save_weights('trained_model_data/trained_model.h5')

def predict(file_name):

    #loading image as a numpy array
    image = Image.open(file_name)
    print("Creating numpy representation of image %s " % file_name)
    resize = image.resize(IMAGE_SIZE, Image.NEAREST)
    resize.load()
    data = np.asarray( resize, dtype="uint8" ) * 1./255
    data = np.expand_dims(data, axis=0)
    #generating model and loading saved weights
    model = generate_model();
    model.load_weights('trained_model_data/trained_model.h5')

    output = model.predict(data)
    print(output)

def predict_folder(folder_name):
    files = []
    for file_name in os.listdir(folder_name):
        if '.png' in file_name:
            files.append(os.path.join(folder_name, file_name))

    data = np.empty([len(files), 50, 50, 3])
    i = 0
    for file_full_name in files:
        #loading image as a numpy array
        image = Image.open(file_full_name)
        resize = image.resize(IMAGE_SIZE, Image.NEAREST)
        resize.load()
        image2 = np.asarray(resize, dtype="uint8" ) * 1./255
        data[i] = image2
        i += 1

    #data = np.expand_dims(data, axis=0)
    #generating model and loading saved weights
    model = generate_model();
    model.load_weights('trained_model_data/trained_model.h5')

    output = model.predict(data)
    print(output)
    print(np.histogram(output, bins=10,range=(0,1)))

#    i = 0
#    file_list = []
#    for file_full_name in files:
#        file_list.append([file_full_name, output[i].item()])
#        i += 1
#
#    file_list.sort(key=lambda x: x[1])
#
#    with open('output_template.html', 'r') as template_file:
#        template = Template(template_file.read())
#        rendered_template = template.render({'output': file_list})
#        with open("training_output.html", "w") as fh:
#            fh.write(rendered_template)

def predict_all():
    test_datagen = ImageDataGenerator(rescale=1./255)
    validation_generator = test_datagen.flow_from_directory(
            'data-scrubbed/validation',
            target_size=IMAGE_SIZE,
            batch_size=BATCH_SIZE,
            class_mode='binary')
    model = generate_model()
    output = model.predict_generator(
                validation_generator,
                2000 / BATCH_SIZE)
    print(output.size)

if __name__ == '__main__':
    parser = OptionParser()
    (options, args) = parser.parse_args()
    if args[0] == 'fit':
        fit()
    elif args[0] == 'predict':
        predict(args[1])
    elif args[0] == 'predict_all':
        predict_all()
    elif args[0] == 'predict_folder':
        predict_folder(args[1])
    else:
        print('Improper usage.  Options are fit or predict')

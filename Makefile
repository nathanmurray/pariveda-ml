run:
	docker run -it -v $(shell pwd):/home -w /home nathanmurray/pariveda-ml

build:
	docker build -t nathanmurray/pariveda-ml .


run-gpu:
	nvidia-docker run -it --entrypoint=/bin/bash -v $(pwd):/home tensorflow/tensorflow:latest-gpu --login

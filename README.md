### Getting Started ###

The easiest way to get started is to clone this repository and use the prebuilt Docker image to run some of the examples.  Fire up the Terminal (Mac) or cmd.exe (Windows) and clone this repository.

```
git clone https://bitbucket.org/nathanmurray/pariveda-ml.git
```

Now pull the docker image.  It may take a few minutes depending on your connection.  If you don't yet have Docker installed, you can find instructions [here](https://docs.docker.com/engine/installation/).

```
docker pull nathanmurray/pariveda-ml
```

You're now ready to run your first command.  If you're on a Mac, make sure you are still in the same working directory that contains the pariveda-ml project and run the code below.  For those interested, the `-v` mounts a folder from your local filesystem into the Docker container; the `$(pwd)` tells it to use the current working directory.

```
docker run -it -v $(shell pwd):/home -w /home nathanmurray/pariveda-ml
```

On Windows, it's easiest to use PowerShell for the similar command:

```
docker run -it -v $($pwd.Path + ":/home") -w /home nathanmurray/pariveda-ml
```
### Questions / Comments ###

This repo is in it's infancy and I'm still building it out.  Feel free to reach out to me on Slack `@nathan.murray` if you run into any issues.  Or better yet, put in some more examples and contribute!

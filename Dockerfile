FROM ubuntu:16.04

LABEL maintainer "nathan.murray@parivedasolutions.com"

RUN apt-get update --fix-missing && apt-get install -y wget bzip2

RUN echo 'export PATH=/opt/conda/bin:$PATH' > /etc/profile.d/conda.sh && \
    wget --quiet https://repo.continuum.io/miniconda/Miniconda3-4.3.11-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p /opt/conda && \
    rm ~/miniconda.sh

ENV PATH /opt/conda/bin:$PATH

RUN pip install tensorflow==1.0.0

RUN pip install keras==2.0.2

RUN pip install Pillow==4.0.0

RUN pip install h5py==2.7.0

RUN pip install Jinja2==2.9.5

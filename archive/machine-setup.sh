#install Azure tools

#first need to install Node JS
curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
sudo apt-get install -y nodejs
#now install azure tools
sudo npm install -g azure-cli

docker-machine create --driver azure \
                      --azure-subscription-id 65fcd321-c697-4cfd-8441-6bd3973c284b \
                      --azure-location southcentralus \
                      --azure-size Standard_NC6 \
                      --azure-image canonical:UbuntuServer:16.04.0-LTS:latest \
                      azure08
					  
					  
docker-machine ssh azure07

##########
#following needs to take place on docker machine
##########

# Install official NVIDIA driver package
sudo apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub
sudo sh -c 'echo "deb http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64 /" > /etc/apt/sources.list.d/cuda.list'
sudo apt-get update && sudo apt-get install -y --no-install-recommends cuda-drivers


# Unload nouveau - these are open source drivers for NVIDIA graphics cards that need to be disabled
sudo rmmod nouveau


# Install nvidia-docker and nvidia-docker-plugin
wget -P /tmp https://github.com/NVIDIA/nvidia-docker/releases/download/v1.0.0/nvidia-docker_1.0.0-1_amd64.deb
sudo dpkg -i /tmp/nvidia-docker*.deb && rm /tmp/nvidia-docker*.deb


#now back on the main machine
eval `docker-machine env azure01`
export NV_HOST="ssh://docker-user@$(docker-machine ip azure01):"


eval `ssh-agent -s`
ssh-add ~/.docker/machine/machines/azure01/id_rsa



# To Dos #

 - Modify docker image working directory so that /code does not have to be referenced between files
 - Make the docker image public
 - Move data into it's own directory.  Consider a new directory for every example
 - Create a Keras example
 - Create a raw Tensorflow example - Inception
 - Potentially build a Deeplearning4j model?
 - Build a model based on Kerry Stover's weekly emails
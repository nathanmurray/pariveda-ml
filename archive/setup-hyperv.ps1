################
# create a new Ubuntu virtual machine for use with nvidia-docker
################

################
# To use, replace VMStoragePath and VHDStoragePath variables with the proper path, then run in powershell
################

$VMName = 'ubuntu-16.04.2-server-amd64'
$VHDName = '{0}.vhdx' -f $VMName
$VMStoragePath = 'D:\Hyper-V\ISOs'
$VHDStoragePath = Join-Path -Path 'D:\Hyper-V\vms\Virtual Hard Disks' -ChildPath $VHDName
$UbuntuISOPath = 'D:\Hyper-V\ISOs\ubuntu-16.04.2-server-amd64.iso'
 
New-VM -Name $VMName -MemoryStartupBytes 512MB -SwitchName vSwitch -BootDevice CD -Path $VMStoragePath -Generation 1 -NoVHD
Set-VMMemory -VMName $VMName -DynamicMemoryEnabled $true -MinimumBytes 256MB -MaximumBytes 1GB
Set-VMProcessor -VMName $VMName -Count 2
New-VHD -Path $VHDStoragePath -SizeBytes 40GB -Dynamic -BlockSizeBytes 1MB
Add-VMHardDiskDrive -VMName $VMName -ControllerType IDE -ControllerNumber 0 -ControllerLocation 0 -Path $VHDStoragePath
Set-VMDvdDrive -VMName $VMName -ControllerNumber 1 -ControllerLocation 0 -Path $UbuntuISOPath
Add-ClusterVirtualMachineRole -VMName $VMName